package ru.chelnokov.helpMe;

import java.util.Scanner;

/**
 * Класс, выясняющий, не является ли введённое
 * слово или фраза палиндромом - выражением,
 * которое одинаково читается слева направо и справа
 * налево.
 *
 * @author Chelnolov E.I., 16IT18k
 */
public class Palindrome {
    private static Scanner reader = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите слово или фразу");
        String phrase = reader.nextLine();
        isCorrect(phrase);
        String withoutSpace = phrase.toLowerCase().replaceAll("[^a-zа-я0-9]", "");
        String reversePhrase = new StringBuffer(withoutSpace).reverse().toString();
        isPalindrome(withoutSpace, reversePhrase);
    }

    /**
     * проверяет корректность ввода данных
     * @param phrase - вводимое выражение
     */
    private static void isCorrect(String phrase) {
        while (phrase.equals("")) {
            System.out.println("Строка пустая или данные некорректны. Введите строку или фразу");
            phrase = reader.nextLine();
        }
    }

    /**
     * выясняет, является ли выражение палиндромом
     * @param phrase - исходное выражение
     * @param reversePhrase - выражение, написанное наоборот
     */
    private static void isPalindrome(String phrase, String reversePhrase) {
        if (reversePhrase.equals(phrase)) {
            System.out.println("Это палиндром");
        } else {
            System.out.println("Не палиндром");
        }
    }
}
